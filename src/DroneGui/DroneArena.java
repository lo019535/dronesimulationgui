package DroneGui;

import java.util.ArrayList;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import javax.swing.JFileChooser;

/**
 * @author tomprobert
 * where drones are
 */



public class DroneArena {
	//pre-declares the necessary values
	private int arenaSizeX;
	private int arenaSizeY;
	private ArrayList<Drone> droneList = new ArrayList<Drone>();
	private ArrayList<Predator> predatorList = new ArrayList<Predator>();
	private ArrayList<Obstacle> obstacleList = new ArrayList<Obstacle>();
	Random randomGenerator;
	//the constructor which when DroneArena is initialised sets the size of the arena
	
	/**
     * Initialise arena with given size
     * @param x int width
     * @param y int length
     */
	public DroneArena(int x, int y)
	{
		arenaSizeX = x;
		arenaSizeY = y;
		randomGenerator = new Random();	
		
	}
	

    /**
     * show all the entities in the interface
     * @param c	the canvas in which drones are drawn

     */

	public void showDrones(MyCanvas c) {
		for(Predator d: predatorList)
		{
			//loop through predatorList and display
			d.displayDrone(c);
		}
		for(Obstacle d: obstacleList)
		{
			//loop through obstacleList and display
			d.displayDrone(c);
		}
		for (Drone d : droneList) 
		{       
			//loop through droneList and display
			d.displayDrone(c);
		}
	}
	
    /**
     * add drone to arena
     * @param droneType parameter for type of drone
     */
	
	public void addDrone(char droneType)
	{	
		int posX,posY;
		boolean clash = true;
		posX = randomGenerator.nextInt(arenaSizeX);		
        posY = randomGenerator.nextInt(arenaSizeY);		

		if (droneList.size() < arenaSizeX * arenaSizeY) {
			//while the drone isnt at the position already taken.

	        while(clash) {
	            if(getDroneAt(posX, posY) == null) {    
	                clash = false;
	            }
	            posX = randomGenerator.nextInt(arenaSizeX);		
	            posY = randomGenerator.nextInt(arenaSizeY);		

	        }

		}
	    if(droneType == 'd'){            
	    	//add Entity of type drone to list
            Drone newEntity = new Drone(posX, posY);
            droneList.add(newEntity);
        }
        else if(droneType == 'o'){       
        	//add Entity of type obstacle to list
            Obstacle newEntity = new Obstacle(posX, posY);
            obstacleList.add(newEntity);
        }
        else if(droneType == 'p'){       
        	//add Entity of type predator to list
            Predator newEntity = new Predator(posX, posY);
            predatorList.add(newEntity);
        }

		
	}


	//Adding a specific entity
	   /**
	 * @param col colour
	 * @param x xpos
	 * @param y yos
	 * @param ID entity id
	 * @param speed speed of entity
	 * @param angle angle of entity
	 */
	public void addEntity(char col, double x,double y, int ID, double speed, double angle){
	        if (col == 'b'){        
	        	//if drone recreate that drone
	            Drone newEntity = new Drone(x,y);
	            newEntity.setDirection(speed,angle);
	            newEntity.setID(ID);
	            droneList.add(newEntity);
	        }
	        else if(col == 'g'){        
	        	//if obstacle recreate that obstacle
	            Obstacle newEntity = new Obstacle(x, y);
	            newEntity.setID(ID);
	            obstacleList.add(newEntity);
	        }
	        else if (col == 'r'){     
	        	//if predator recreate that predator
	            Predator newEntity = new Predator(x,y);
	            newEntity.setDirection(speed,angle);
	            newEntity.setID(ID);
	            predatorList.add(newEntity);
	        }
	    }


	    /**
	     * search arraylist of drones then arraylist of predators to see if there is a drone at x,y
	     * @param x xpos
	     * @param y ypos
	     * @return null if no Drone there, otherwise return drone, return e if there is a predator on the drone.
	     */

	   public Entities getDroneAt(double x, double y) {
		   
			for (Obstacle d : obstacleList) 
			{
				if(d instanceof Obstacle) 
				{
					if (d.isHere(x, y)) 
					{
						return d;        //if drone there, return that drone
					}
				}
			}
			for (Drone d : droneList) 
			{
				for (Predator e : predatorList)
				{
					//loops through at drones and predators in array and runs isHere to see drone locations
					if (d.isHere(x, y)) 
					{
						if (e.isHere(x, y)) 
						{
							return e;
						}
						return d; 
						// returns drone if X & Y same
					}
				}
			}	
			return null;
	   }

	   /**
	     * Checks if area is free for drone to move into
	     * @param x location x
	     * @param y location y
	     * @param current calls the entity required to move
	     * @return true space is available to move into, false if can't
	     */

	   public boolean canMoveHere(double x, double y, Entities current)
		{
	       if(x < arenaSizeX && x > 0 && y < arenaSizeY && y > 0) {

	            if(getDroneAt(x, y) == null || getDroneAt(x, y) == current) {
	                return true;
	            }
	        }
	       return false;
		}
	   
	   /**
	     * try to move all drones and checks if predator can kill 
	     */

		public void moveAllDrones()
		{
			for(Drone d:droneList)
			{
				d.tryToMove(this);
			}
			for(Predator d:predatorList)
			{
				d.tryToMove(this);
				Death();
			}
		}
		
		 /**
	     * function to kill drone.
	     */
		
		public void Death() {
			//loops through drones
			for (Predator p : predatorList) 
			{
				for (Drone d : droneList) 
				{
					//checks to see if they are touching
					if (p.isTouching(d.getPosX(), d.getPosY()))
					{
						//remove drone from dronelist array
						droneList.remove(d);
						
					}
				}
			}
		}
		

		/**
		 * Provide data to save arena
		 * @return string with arena and entity info on each line
		 */
		
		public String saveArenaData() {
			String save = "";
		
			save += arenaSizeX + "\n";
			save += arenaSizeY;
			for (Drone d : droneList)  //foreach drone in droneList call their save drone function
			{
			    save += "\n";
			    save += d.saveDrone();
			}
			for (Predator p : predatorList) 
			{
			    save += "\n";
			    save += p.saveDrone();
			}
			for (Obstacle o : obstacleList) 
			{
			    save += "\n";
			    save += o.saveDrone();
			}
			return save;
		}

	    /**
	     * return size of arena and location of drones
	     */

	   public String toString() {
		   
	        String locations = "[                          Drone Arena Combat                           ]\n" + "The arena is " + arenaSizeX + " x " + arenaSizeY;//arena info
	        //drone info
	        for (Obstacle o : obstacleList) {
	            locations += "\n";
	            locations += o.toString();
	        }
	        for (Predator p : predatorList) {
	            locations += "\n";
	            locations += p.toString();
	        }
	        for (Drone d : droneList) {
	            locations += "\n";
	            locations += d.toString();
	        }

	        return locations; 
	    }


	    /**
	     * saves arena to file found with JFileChooser.
	     */

	   public void saveArena()
	   {
		   //pick name and location with JFileChooser
		   //start a thread.
	        Thread t1 = new Thread(new Runnable() {
	            @Override
	            public void run() {
	                JFileChooser chooser = new JFileChooser();
	                int approve = chooser.showSaveDialog(null);
	                //if possible
	                if (approve == JFileChooser.APPROVE_OPTION) 
	                {		
	                	//get output file
	                    File selectFile = chooser.getSelectedFile();			
	                    //try to save files by creating filewriter for selFile
	                    try {
	                        FileWriter outFileWriter = new FileWriter(selectFile);		
	                        PrintWriter writer = new PrintWriter(outFileWriter);
	                        writer.println(saveArenaData());						
	                        writer.close();
	                    }
	                    catch (FileNotFoundException e) {							
	                        e.printStackTrace();
	                    }
	                    catch (IOException e) {									
	                        e.printStackTrace();
	                    }
	                }
	            }
	        });
	        t1.start();


	 }

	
}
