package DroneGui;

/**
 * @author tomprobert
 * Abstract class that others can use.
 */

public abstract class Entities {
	
    protected double posX;
    protected double posY;
    protected int size;
    protected char colour;

    /**
     * Constructor initilises variables
     * @param X xposition
     * @param Y yposition
     */
    public Entities(double X, double Y) 
    {
        posX = X; // x and y location
        posY = Y;
    }
    
    /**
     * check whether entity is at location
     * @return true if there false if not
     * @param x xpos
     * @param y ypos
     */
    
    public boolean isHere(double x, double y) 
    {
        //include size of entity
        double top = y+size;
        double bottom = y-size;
        double left = x-size;
        double right = x+size;
        //check if there
        if((posX < right && posX > left) && (posY < top && posY > bottom)) 
        {
            return true;
        }

        return false;
    }

    /**
     * display the piece in the canvas
     * @param c	the canvas used
     */

    public void displayDrone(MyCanvas c)
    {
        c.showCircle(posX, posY, size, colour);
    }
    
    /**
     * getter for position x
     * @return x xpos
     */

    public double getPosX() {
		return posX;
	}
    
	/**
	 * getter for position y
	 * @return y ypos
	 */
	public double getPosY() {
		return posY;
	}

    /**
     * getter for size of entity
     * @return int entity size
     */

	public int getSize()
    {
    	return size;
    }

    /**
     * get colour of entity
     * @return char colour of entity
     */

    public char getColour()
    {
    	return colour;
    }
    
    /**
     * check if can move, abstract
     * @param a drone arena
     */

    public abstract void tryToMove(DroneArena a);

    public abstract String toString();
    


}
