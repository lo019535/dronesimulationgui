package DroneGui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * @author tomprobert
 * where to make gui and load file
 */

public class DroneBorderPane extends Application {
	
	private VBox rtPane;						// pane in which info on system listed
    private int canvasSize = 512;				// size of canvas
    private DroneArena arena;					// drone arena
    private MyCanvas mc;						// canvas into which system drawn	
	private boolean animStatus = false;
   
	 /**
	 *function to show a message will be pinned to right
     * @param TStr title of message block
     * @param CStr content of message
     */
	
	private void showMessage(String TStr, String CStr) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(TStr);
		alert.setHeaderText(null);
		alert.setContentText(CStr);
		
		alert.showAndWait();
	}
	
	 /**
     * function to show in a box About the program
     */
	
	private void showAbout() {
		showMessage("About", "My Drone Simulation");
	}
	
	 /**
     * function to open a file and ability to load
     */
	
	 public void openFile() 
	 {
		 //create new thread and load filechooser
	        Thread t1 = new Thread(new Runnable() 
	        {
	            @Override
	            public void run() 
	            {
	                JFileChooser chooser = new JFileChooser();
	                int approve = chooser.showOpenDialog(null);
	                if (approve == JFileChooser.APPROVE_OPTION) 
	                {				
	                	//selected file
	                    File selFile = chooser.getSelectedFile();				
	                    if(selFile.isFile())
	                    { 
	                    	//exists and is a file set up file and stream
	                        try {
	                        	
	                            String path = chooser.getSelectedFile().getPath(); // full path
	    					    Path pathToFile = Paths.get(path); //get path to file
	                            long lineCount = Files.lines(pathToFile).count(); //get total line count
	                            double x = 0; //create variables for each object
	                            double y = 0;
	                            int ID = 0;
	                            char colour = 'd';
	                            double speed = 0;
	                            double angle = 0;
	                            
	                            int a = Integer.parseInt(Files.readAllLines(pathToFile).get(0),10); //set integer a to arena x
	    						int b = Integer.parseInt(Files.readAllLines(pathToFile).get(1),10); //set integer b to arena y
	    						arena = new DroneArena(a, b); //create a new arena with a and y
	    						for(int i = 2; i < lineCount; i=i+6) //for i till line count add 6
	    						{
	    							colour = Files.readAllLines(pathToFile).get(i).charAt(0); //line is put to a char
	    					        ID = Integer.parseInt(Files.readAllLines(pathToFile).get(i+1),10); //id is parsed to an Int
	    					        x = Double.parseDouble(Files.readAllLines(pathToFile).get(i+2)); //x,y,speed and angle are parsed to double
	    					        y = Double.parseDouble(Files.readAllLines(pathToFile).get(i+3));
	    					        speed = Double.parseDouble(Files.readAllLines(pathToFile).get(i+4));
	    					        angle = Double.parseDouble(Files.readAllLines(pathToFile).get(i+5));
	    					        arena.addEntity(colour, x, y, ID, speed, angle); //Entity is created in arena
	    						}                    
	                            displaySystem(); //display system
	                        }
	                        
	                        catch (FileNotFoundException e) //if cant find file
	                        {
	                            e.printStackTrace();
	                        }
	                        catch (IOException e)
	                        {
	                            e.printStackTrace();
	                        }
	                    }
	                }
	            }
	        });
	        t1.start();

	 }
		/**
		 * Function to set up the menu
		 */
	 
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar(); //create menu

		Menu mHelp = new Menu("Help"); //have entry for help
				//then add sub menus for About and Help
				//add the item and then the action to perform
		MenuItem mAbout = new MenuItem("About");
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout(); //show the about message
            }	
		});
		mHelp.getItems().addAll(mAbout); //add submenus to Help
		
		//now add File menu, which here only has Exit
		Menu mFile = new Menu("File");
		MenuItem mExit = new MenuItem("Exit");
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		        System.exit(0);						
		        //quit program
		    }
		});
		
		//add save option
        MenuItem mSave = new MenuItem("Save");
        mSave.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                arena.saveArena(); //save arena
            }
        });

        //add load option
        MenuItem mLoad = new MenuItem("Load Arena");
        mLoad.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                openFile();  //load arena
            }
        });
		
		mFile.getItems().addAll(mSave, mLoad, mExit); //add save load and exit
		
		menuBar.getMenus().addAll(mFile, mHelp); //menu has File and Help
		
		return menuBar;	 //return the menu, so can be added
	}
	/**
	 * show where entities are, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear(); // clear rtPane
		Label label = new Label(arena.toString()); // get label which has information on system - use ourSystem.toString()
		rtPane.getChildren().add(label); // add label to rtPane
	}
	
	
    /**
     * displays system
     */
    public void displaySystem() { //used to display the drone arena
        mc.clearCanvas(); //clear canvas
        arena.showDrones(mc);  //show entities
        drawStatus();
    }

    /**
     * set up the buttons and return so can add to borderpane
     *
     * @return new HBox
     */
	
	private HBox setButtons() {
// create button
		Button animOn = new Button("Start Simulation");
		// now add handler
		animOn.setOnAction(new EventHandler<ActionEvent>() {
		@Override
			public void handle(ActionEvent event) {
				animStatus = true;
			}
		});
		
		 Button animOff = new Button("Stop Simulation");
	      // now add handler
		 animOff.setOnAction(new EventHandler<ActionEvent>() {
		     @Override
	         public void handle(ActionEvent event) {
		    	 animStatus = false;
		     }
		 });

		// create button
	        Button newDroneButton = new Button("New Drone");
	        // now add handler
	        newDroneButton.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	                arena.addDrone('d');
	                displaySystem();
	            }
	        });
	        
	        // create button
	        Button newObsButton = new Button("New Obstacle");
	        // now add handler
	        newObsButton.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	                arena.addDrone('o');
	                displaySystem();
	            }
	        });

	        // create button
	        Button newPredatorButton = new Button("New Predator");
	        // now add handler
	        newPredatorButton.setOnAction(new EventHandler<ActionEvent>() {
	            @Override
	            public void handle(ActionEvent event) {
	                arena.addDrone('p');
	                displaySystem();
	            }
	        });

	        return new HBox(animOn, animOff, newDroneButton, newObsButton, newPredatorButton);
	    }

    /**
     * start function sets up canvas, menu, buttons and timer
     */

	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("Tom Probert's Drone Simulation");

	    BorderPane bp = new BorderPane();			// create border pane

	    bp.setTop(setMenu());						// create menu, add to top

	    Group root = new Group();					// create group
	    Canvas canvas = new Canvas(canvasSize, canvasSize);
	    											// and canvas to draw in
	    root.getChildren().add( canvas );			// and add canvas to group
	    mc = new MyCanvas(canvas.getGraphicsContext2D(), canvasSize, canvasSize);
					// create MyCanvas passing context on canvas onto which images put
	    arena = new DroneArena(canvasSize, canvasSize);
	    
	    bp.setCenter(root);							// put group in centre pane

	    rtPane = new VBox();						// set vBox for listing data
	    bp.setRight(rtPane);						// put in right pane

	    bp.setBottom(setButtons());					/// add button to bottom

	    Scene scene = new Scene(bp, canvasSize*2, canvasSize*1.2);
	    								// create scene so bigger than canvas and can fit in all of arena.toString, 
	    new AnimationTimer()			// create timer
        {
            public void handle(long currentNanoTime) {
                // define handler for what do at this time
                if (animStatus) {
                    arena.moveAllDrones();                    // move drones
                    displaySystem();                            // now clear canvas and draw system							// now clear canvas and draw system
                }
            }
        }.start();


	    stagePrimary.setScene(scene);
	    stagePrimary.show();
	}

	/**
	 * @param args basic main function
	 */
	public static void main(String[] args) {
	    Application.launch(args);
	}

}
