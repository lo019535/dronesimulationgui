package DroneGui;

/**
 * @author tomprobert
 * Obstacles class
 *
 */
public class Obstacle extends Entities {
	int speed = 0;
	int angle = 0;
    protected static int start = 0;
    protected int ID = 0;
    
    /**
     * Constructor
     * @param X position x
     * @param Y position y
     */
    public Obstacle(double X, double Y) {
        super(X, Y);
        //all obstacles have set size and colour
        size = 10;
        colour = 'g'; //green for tree
        ID = start++;
    }

    
    /**
     *returns obstacle info
     */
    @Override
    public String toString() {
        return "Obstacle " + ID + " is at " + posX + ", " + posY;
    }

	 /**
	 * @return save information
	 */
	public String saveDrone() {
	        String save = "";
	        save += colour + "\n";
	        save += ID + "\n";
	        save += posX + "\n";
	        save += posY + "\n";
	      
	        save += speed + "\n";
	        save += angle;
	        return save;

	    }
	
    /**
     *function required as extension of entities.
     */
    @Override
    public void tryToMove(DroneArena a) {

    }
    
    
    /**
     * @param givenID
     * sets id to givenId
     */
    public void setID(int givenID){
        ID = givenID;
    }
    

}
