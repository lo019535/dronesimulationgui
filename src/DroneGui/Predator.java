package DroneGui;

/**
 * @author tomprobert
 *
 */
public class Predator extends Drone{
	 protected int ID;
	 protected static int starter = 0;
	/**
	 * Constructor presets values
	 * @param X position X
	 * @param Y position Y
	 */
	public Predator(double X, double Y) {
        super(X, Y);
        speed = 4;
        size = 6;
        colour = 'r';
        ID = starter++;
        start--; //-- needed as start added each time due to it being inherited from drone constructor being called
		// TODO Auto-generated constructor stub
	}
	

	/**
	 *@param x position x
	 *@param y position y
	 *@return true if here false if not
	 */
	@Override
    public boolean isHere(double x, double y) {
        //include size of entity + extra radius
        double top = y+size+size*2;
        double bottom = y-size-size*2;
        double left = x-size-size*2;
        double right = x+size+size*2;
        if((posX <= right && posX >= left) && (posY <= top && posY >= bottom)) { //check if there
            return true;
        }

        return false;
    }

	/**
	 *@return save all values
	 */
	public String saveDrone() {
	        String save = "";
	        save += colour + "\n";
	        save += ID + "\n";
	        save += posX + "\n";
	        save += posY + "\n";
	        save += speed + "\n";
	        save += angle;
	        return save;

	    }
	
    /**
     *@return string of predator
     */
    @Override
    public String toString() {
        return "Predator " + ID + " is at " + String.format("%.2f",posX) + "," + String.format("%.2f",posY) + " with angle " + String.format("%.2f",angle);
    }
    
    /**
     *@param givenID to set id
     */
    public void setID(int givenID){
        ID = givenID;
    }

}
