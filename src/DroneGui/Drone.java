package DroneGui;
import java.util.Random;


/**
 * @author tomprobert
 * drone class
 */
public class Drone extends Entities {
	
	protected double speed = 2;           //speed of all drones is 2
    protected double angle;
    protected int ID;
    protected static int start = 0;
	/**
	 * @param X xpos
	 * @param Y ypos
	 */
	public Drone(double X, double Y) {
		super(X, Y);
        size = 3;
        colour = 'b';
        ID = start++; //unique ID
        //add a random direction
        Random rand = new Random(System.currentTimeMillis());
        angle = rand.nextFloat()*2*Math.PI;
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * function to check if the drone can move.
	 *@param a dronearena
	 */
	@Override
	public void tryToMove(DroneArena a) {
		
		double newX = posX + speed*Math.cos(angle);
	    double newY = posY + speed*Math.sin(angle);
	    Random rand = new Random();
	    double randangle = 90 + (360 - 90) * rand.nextDouble();
        //if can move there
        if(a.canMoveHere(newX, newY, this)) {
            posX = newX;
            posY = newY;
        }
        else {
            //bounce back
            angle += Math.toRadians(randangle);
            //move in that direction
            newX = posX + speed*Math.cos(angle);
            newY = posY + speed*Math.sin(angle);

            //update positions
            posX = newX;
            posY = newY;
        }

	}
	
	/**
	 * function to get string of the drone
	 *@return save at end of function
	 */
	public String saveDrone() {
		String save = "";
		save += colour + "\n";
		save += ID + "\n";
		save += posX + "\n";
		save += posY + "\n";
		//extra lines where speed and angle added for drone
		save += speed + "\n";
		save += angle;
		
		return save;
	
	}
   
    /**
     * @param x xpos
     * @param y ypos
     * @return true if touching false if not
     */
    public boolean isTouching(double x, double y) {
		int radius = 20;
		if ((posX - x < radius && posX - x > -radius) && (posY - y < radius && posY - y > -radius)) 
		{
			return true; 
		}
		return false;
		
    }

	/**
	 * @param spe speed
	 * @param ang angle
	 */
	public void setDirection(double spe, double ang){
	    speed = spe;
	    angle = ang;
	}


	//drone toString func to give id position and direction.
	/**
	 *@return drone information
	 */
	public String toString() {
		return "Drone " + ID + " is at " + String.format("%.2f",posX) + "," + String.format("%.2f",posY) + " with angle " + String.format("%.2f",angle);
	}
	
	   /**
     * allows setting of ID
     * @param givenID shows given id
     */

    public void setID(int givenID){
        ID = givenID;
    }
}
