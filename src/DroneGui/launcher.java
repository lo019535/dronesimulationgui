package DroneGui;


/**
 * @author tomprobert
 * main function launches DroneBorderPane
 */
public class launcher {


	/**
	 * @param args main function for launcher send to droneborderpane main
	 */
	public static void main(String[] args) 
	{ 
		DroneBorderPane.main(args);
	}

}
