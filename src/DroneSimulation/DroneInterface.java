package DroneSimulation;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.lang.Integer;

/**
 * Simple program to show arena with multiple drones
 * @author shsmchlr
 *
 */
public class DroneInterface {
	
	private Scanner s;								// scanner used for input from user
	private DroneArena myArena;				// arena in which drones are shown
	private ConsoleCanvas myCanvas;
	private int x,y;
	private String fileName;
	private Direction dir;
	String str;
/**
	 * constructor for DroneInterface
	 * sets up scanner used for input and the arena
	 * then has main loop allowing user to enter commands
 */
	public DroneInterface() {
		s = new Scanner(System.in);			// set up scanner for user input
		myArena = new DroneArena(0, 0);	// create arena of size 0*0
		File directory;
		char ch = ' ';
		do {
			System.out.print("(A)dd drone "
					+ "\n(I)nformation "
					+ "\n(D)rone Display "
					+ "\n(M)ove all drones "
					+ "\n(C)reate new Arena"
					+ "\n(T)en Movements"
					+ "\n(S)ave data"
					+ "\n(L)oad Data"
					+ "\ne(X)it             > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			//Case to add drone
				case 'A' :
				case 'a' :
					myArena.addDrone();	// add a new drone to arena
					System.out.println("Drone Added");
					break;
			//Case to give information about Arena and drones
				case 'I' :
				case 'i' :
					System.out.println(myArena.toString());
					break;
			//Case to Display the arena
				case 'D' :
				case 'd' :
					System.out.println(myArena.toString());
					doDisplay();
					break;
			//Case to move all the drones once
				case 'M' :
				case 'm' :
					myArena.moveAllDrones();
					System.out.println(myArena.toString());
					doDisplay();
					break;
			//Case to move all the drones 10 times in an animation.
				case 'T' :
				case 't' :
					for(int i = 0; i < 10; i++)
					{
						try {
							myArena.moveAllDrones();
							System.out.println(myArena.toString());
							doDisplay();
							Thread.sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					System.out.println("Animation ran 10 times");
					break;
			//Case to Create a new Arena asks use for x and y then makes it.
				case 'C' :
				case 'c' :
					System.out.print("Enter the x of the arena > ");
					x = s.nextInt();
					s.nextLine();
					System.out.print("Enter the y of the arena > ");
					y = s.nextInt();
					s.nextLine();
					makeArena(x,y);
					break;
			//Case to create a save file of the Arena
				case 'S':
				case 's':
					directory = new File("C:/Users/tomis/eclipse-workspace/DroneSimulation/bin");
					System.out.print("Create name of save file > ");
					fileName = s.nextLine();
					
					//check if name exists if so add ( number )
					String filenames[] = directory.list();
					if (directory.length() != 0)
					{
						for (int i = 0; i < filenames.length; i++) {
							if(fileName == filenames[i])
							{
								fileName = fileName + "(" + i + ")";
							}
						}
					}
					System.out.print("name of file = " + fileName + "\n");
					
					//save it all to file
					File outFile = new File("C:/Users/tomis/eclipse-workspace/DroneSimulation/bin/" + fileName + ".txt");
					try {
						FileWriter outFileWriter = new FileWriter (outFile);
						PrintWriter writer = new PrintWriter(outFileWriter);
						writer.println(myArena.getX());
						writer.println(myArena.getY());
						for(Drone d : myArena.getDroneList())
						{
							writer.println(d.getVersion());
							writer.println(d.getX());
							writer.println(d.getY());
							writer.println(d.getDirection());
						}
						writer.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					
					break;
			//Case to load questioned file
				case 'L' :
				case 'l' :
					//Loading the file
					System.out.print("Enter name of save file to load > ");
					fileName = s.nextLine();
					String file = fileName + ".txt";
					System.out.print("name of save file = " + fileName + "\n");
					try {
						
						int droneX,droneY,droneVersion;
					    Path pathToFile = Paths.get("C:/Users/tomis/eclipse-workspace/DroneSimulation/bin/" + file);
					    System.out.println(pathToFile.toAbsolutePath());
				        long lineCount = Files.lines(pathToFile).count();
						int a = Integer.parseInt(Files.readAllLines(pathToFile).get(0),10);
						int b = Integer.parseInt(Files.readAllLines(pathToFile).get(1),10);
						//System.out.println(a + " " + b);
						makeArena(a,b);
						for(int i = 2; i < lineCount; i=i+4)
						{

					        droneVersion = Integer.parseInt(Files.readAllLines(pathToFile).get(i),10);
					        droneX = Integer.parseInt(Files.readAllLines(pathToFile).get(i+1),10);
					        droneY = Integer.parseInt(Files.readAllLines(pathToFile).get(i+2),10);
					        str = Files.readAllLines(pathToFile).get(i+3);
					        //System.out.println("test " + str);
					        //System.out.println(droneVersion + " " + droneX + " " + droneY + " " + str);
							
							myArena.addSpecificDrone(droneVersion, droneX, droneY, str);
							
						}
						doDisplay();
					} catch (IOException e) 
					{
					e.printStackTrace();
					}
					
					break;
				case 'x' : 	ch = 'X';				// when X detected program ends
					break;
			}
		} while (ch != 'X');						// test if end
		s.close();									// close scanner
	}

	//Gives the myArena size x and y then displays it.
	void makeArena(int x, int y)
	{
		myArena.setX(x);
		myArena.setY(y);
		doDisplay();
	}
	
	//Determines the borders of the arena and then displays it
	void doDisplay() 
	{
		// determine the arena size 
		int x = myArena.getX()+2;
		int y = myArena.getY()+2;
		//hence create a suitable sized ConsoleCanvas object
		myCanvas = new ConsoleCanvas(x,y); 
		//call showDrones suitably
		myArena.showDrones(myCanvas);
		//then use the ConsoleCanvas.toString method 
		System.out.println(myCanvas.toString());	
		}

//calls the interface to start it all.
public static void main(String[] args) {
	DroneInterface r = new DroneInterface();	// just call the interface
	}

}
