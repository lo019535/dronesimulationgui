package DroneSimulation;
import java.util.Random;

//Creates the enum for directions
public enum Direction {
	NORTH,
	EAST,
	SOUTH,
	WEST;
//gets a random direction by taking all the values and getting a random one to match the direction
    public static Direction getRandomDirection() 
    {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
   //sets the next direction
	public Direction nextDirection() 
	{
		return values()[(this.ordinal() + 1) % values().length];
	}

	public static void main(String[] args) {
		Direction direction = Direction.getRandomDirection();
		System.out.println(direction);

	}

	
}
