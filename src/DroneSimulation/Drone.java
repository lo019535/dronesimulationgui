/**
 * 
 */
package DroneSimulation;

/**
 * @author tomis
 *
 */
public class Drone {
	//initialised variables
	private int posX, posY,version;
	private static int nextVersion = 1;
	private Direction direction;

	//getters
	public int getX() {
		return posX;
	}
	public int getY() {
		return posY;
	}
	public int getVersion() {
		return version;
	}
	public Direction getDirection() {
		return direction;
	}
	//setters
	public void setVersion(int version) {
		this.version = version;
	}
	//Constructor sets ids and positions with direction
	public Drone(int x, int y, Direction d)
	{
		posX = x;
		posY = y;
		version = nextVersion;
		nextVersion++;
		direction = d;
	}
	//the isHere to check if drone is there.
	public boolean isHere(int sx, int sy)
	{
		if(sx == posX && sy == posY)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//Uses the console canvas to display the drone using a letter D
	public void displayDrone(ConsoleCanvas c) 
	{
		c.showIt(posX, posY, 'D');
	}
	
	//tried to create it so the drone could not possibly leave arena and succeeded elsewhere thought it would be nice to keep in for future reference.
	public boolean check(int x,int y)
	{
		if(posX==0)
		{
			return true;
		}
		if(posY==0)
		{
			return true;		
		}
		if(posX==x+1)
		{
			return true;		
		}
		if(posY==y+1)
		{
			return true;
		}
		return false;
	}
	
	//The attempt to move the drone. has to call the dronearena class. Foreach of the cases, checks if the drone can move there and if so moves, else it changes direction and trys again
	public void tryToMove(DroneArena a) 
	{
		switch (direction) {
		case NORTH:
			if (a.canMoveHere(posX - 1, posY) && posX != 1)
			{
				posX = posX - 1;
				direction = direction.getRandomDirection();
			}
			else
			{
				direction = direction.nextDirection();
			}
			break;
		case EAST:
			if (a.canMoveHere(posX, posY + 1) && posY != a.getY())
			{
				posY = posY + 1;
				direction = direction.getRandomDirection();
			}
			else
			{
				direction = direction.nextDirection();
			}
			break;
		case SOUTH:
			if (a.canMoveHere(posX + 1, posY) && posX != a.getX())
			{
				posX = posX + 1;
				direction = direction.getRandomDirection();
			}
			else
			{
				direction = direction.nextDirection();
			}
			break;
		case WEST:
			if (a.canMoveHere(posX, posY - 1) && posY != 1)
			{
				posY = posY - 1;
				direction = direction.getRandomDirection();
			}
			else
			{
				direction = direction.nextDirection();
			}
			break;
		default:
			break;
		}
	}
	
	//drone toString func to give id position and direction.
	public String toString() {
		return "Drone " + version + " is at " + posX + "," + posY + " with direction " + direction.toString();
	}
	public static void main(String[] args)
	{
		//Drone d = new Drone(5, 2, Direction.getRandomDirection());
		//System.out.println(d.toString()); // print where is
	}

}

